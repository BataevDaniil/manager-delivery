const knexConfig = {
  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      database: 'manager-delivery',
      user: 'codemonx',
      password: 'qwerty',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: `${__dirname}/src/migrations`,
    },
    seeds: {
      directory: './src/seeds',
    },
  },

  production: {
    client: 'mysql',
    connection: {
      host: 'storage',
      database: 'manager-delivery',
      user: 'codemonx',
      password: 'qwerty',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: `${__dirname}/build/migrations`,
    },
    seeds: {
      directory: './build/seeds',
    },
  },
}

module.exports = knexConfig
