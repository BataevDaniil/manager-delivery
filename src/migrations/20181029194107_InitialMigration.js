export const up = (knex, Promise) =>
  Promise.all([
    knex.schema.createTable('Order', table => {
      table.increments('id').primary()
      table.string('firstName')
      table.string('lastName')
      table.string('fatherName')
      table.string('phone')
      table.string('address')
      table.integer('countLabels').unsigned()
      table.integer('createdAt').unsigned()
      table.integer('updatedAt').unsigned()
    }),
    knex.schema.createTable('User', table => {
      table.increments('id').primary()
      table
        .string('email')
        .notNullable()
        .unique()
      table.string('password').notNullable()
      table.integer('createdAt').unsigned()
      table.integer('updatedAt').unsigned()
    }),
  ])

export const down = (knex, Promise) =>
  Promise.all([knex.schema.dropTable('Order'), knex.schema.dropTable('User')])
