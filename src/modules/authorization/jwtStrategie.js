import * as R from 'ramda'
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt'
import passport from 'passport'

import { paramsToContext, ENV, K } from '../../../backend'
import { User } from '../user'

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: ENV.JWT_TOKEN,
    },
    R.compose(
      //
      paramsToContext('token', 'done'),
    )(async ({ token: { id }, done }) => {
      const user = await R.pipe(
        () => User,
        K.query(),
        K.findById(id),
      )()
      done(null, user || false)
    }),
  ),
)
