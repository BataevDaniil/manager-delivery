import passport from 'passport'

import { addMiddleware } from '../../../backend'

export const authenticate = strategy =>
  addMiddleware((req, res, next) =>
    passport.authenticate(strategy, (err, user) => {
      if (err) {
        return next(err)
      }
      if (!user) {
        return res.sendStatus(401)
      }
      req.user = user
      next()
    })(req, res, next),
  )

export const withUser = func => ctx => func({ ...ctx, user: ctx.req.user })
