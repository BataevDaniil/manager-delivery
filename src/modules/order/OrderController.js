import * as R from 'ramda'

import {
  get,
  post,
  patch,
  deleteMethod,
  withBody,
  withParams,
  K,
} from '../../../backend'
import { order } from './selectors'
import Order from './Order'
import {
  newOrderValidator,
  updateOrderValidator,
  paramsIdValidate,
} from './validators'
import { authenticate } from '../authorization'

export const getOrder = R.compose(
  get('/order'),
  //
)(
  R.pipe(
    () => Order,
    K.query(),
    K.alias('order'),
    K.select(order),
    K.orderBy('createdAt'),
  ),
)

export const getOrderById = R.compose(
  get('/order/:id'),
  withParams({ schema: paramsIdValidate }),
)(({ params: { id } }) =>
  R.pipe(
    () => Order,
    K.query(),
    K.alias('order'),
    K.select(order),
    K.findById(id),
  )(),
)

export const addOrder = R.compose(
  post('/order'),
  withBody({ schema: newOrderValidator }),
)(({ body }) =>
  R.pipe(
    () => Order,
    K.query(),
    K.insert(body),
  )(),
)

export const updateOrder = R.compose(
  patch('/order/:id'),
  withBody({ schema: updateOrderValidator }),
  withParams({ schema: paramsIdValidate }),
)(({ body, params: { id } }) =>
  R.pipe(
    () => Order,
    K.query(),
    K.findById(id),
    K.patch(body),
  )(),
)

export const deleteOrder = R.compose(
  deleteMethod('/order/:id'),
  authenticate('jwt'),
  withParams({ schema: paramsIdValidate }),
)(({ params: { id } }) =>
  R.pipe(
    () => Order,
    K.query(),
    K.deleteById(id),
  )(),
)
