import { Model } from 'objection'
import moment from 'moment'

class Order extends Model {
  static tableName = Order.name

  $beforeInsert() {
    this.createdAt = moment().unix()
    this.updatedAt = moment().unix()
  }

  $beforeUpdate() {
    this.updatedAt = moment().unix()
  }
}

export default Order
