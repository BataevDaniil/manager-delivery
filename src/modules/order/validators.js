import Joi from 'joi'

export const newOrderValidator = Joi.object().keys({
  firstName: Joi.string(),
  lastName: Joi.string(),
  fatherName: Joi.string(),
  phone: Joi.string()
    .regex(/^\d{11}$/)
    .allow(null),
  address: Joi.string(),
  countLabels: Joi.number(),
})

export const updateOrderValidator = newOrderValidator

export const paramsIdValidate = Joi.object().keys({
  id: Joi.number().required(),
})
