import fetch from 'node-fetch'

import { pipeP } from '../../utils'

export const addOrder = pipeP(body =>
  fetch('http://localhost:3000/order', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }),
)

export const getOrders = pipeP(
  () =>
    fetch('http://localhost:3000/order', {
      method: 'GET',
    }),
  x => x.json(),
)

export const getOrder = pipeP(
  ({ id }) =>
    fetch(`http://localhost:3000/order/${id}`, {
      method: 'GET',
    }),
  x => x.json(),
)

export const deleteOrder = pipeP(({ id, token }) =>
  fetch(`http://localhost:3000/order/${id}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }),
)
export const updateOrder = pipeP(({ id, order }) =>
  fetch(`http://localhost:3000/order/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(order),
  }),
)
