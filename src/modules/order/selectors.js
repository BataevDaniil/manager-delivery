export const order = [
  'id',
  'firstName',
  'lastName',
  'fatherName',
  'phone',
  'address',
  'countLabels',
  'createdAt',
  'updatedAt',
].map(field => `order.${field}`)
