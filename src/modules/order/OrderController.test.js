import { ManagerSeed, awaitStartServer } from '../../../backend'
import * as ManagerOrder from './ManagerOrder'
import { signin } from '../user/testHelpers'

describe('Order controller', () => {
  beforeEach(async () => {
    await awaitStartServer()
  })
  afterEach(async () => {
    await ManagerSeed.deleteAllData()
  })
  it('should add order', async () => {
    await ManagerSeed.insert({})
    const newOrder = { firstName: 'Daniil', address: 'addressTest' }
    await ManagerOrder.addOrder(newOrder)

    const data = await ManagerSeed.getAllData()
    expect(data.Order).toContainEqual(expect.objectContaining(newOrder))
  })
  it('should get all orders', async () => {
    const orders = [
      { firstName: 'Daniil1', address: 'addressTest1' },
      { firstName: 'Daniil2', address: 'addressTest2' },
      { firstName: 'Daniil3', address: 'addressTest3' },
    ]
    await ManagerSeed.insert({
      Order: orders,
    })
    const data = await ManagerOrder.getOrders()
    expect(data).toEqual(
      expect.arrayContaining(orders.map(expect.objectContaining)),
    )
  })
  it('should get order by id', async () => {
    const orders = [
      { id: 1, firstName: 'Daniil1', address: 'addressTest1' },
      { id: 2, firstName: 'Daniil2', address: 'addressTest2' },
      { id: 3, firstName: 'Daniil3', address: 'addressTest3' },
    ]
    await ManagerSeed.insert({
      Order: orders,
    })
    const data = await ManagerOrder.getOrder({ id: 2 })
    expect(data).toMatchObject(orders[1])
  })
  it('should delete order by id', async () => {
    const orders = [
      { id: 1, firstName: 'Daniil1', address: 'addressTest1' },
      { id: 2, firstName: 'Daniil2', address: 'addressTest2' },
      { id: 3, firstName: 'Daniil3', address: 'addressTest3' },
    ]
    const { user, token } = signin()
    await ManagerSeed.insert({
      Order: orders,
      User: [user],
    })
    await ManagerOrder.deleteOrder({ id: 2, token })
    const data = await ManagerSeed.getAllData()
    expect(data.Order).not.toContainEqual(expect.objectContaining(orders[1]))
  })
  it('should update order by id', async () => {
    const orders = [
      { id: 1, firstName: 'Daniil1', address: 'addressTest1' },
      { id: 2, firstName: 'Daniil2', address: 'addressTest2' },
      { id: 3, firstName: 'Daniil3', address: 'addressTest3' },
    ]
    await ManagerSeed.insert({
      Order: orders,
    })
    await ManagerOrder.updateOrder({
      id: 2,
      order: { address: 'newAddressTest2' },
    })
    orders[1].address = 'newAddressTest2'
    const data = await ManagerSeed.getAllData()
    expect(data.Order).toContainEqual(expect.objectContaining(orders[1]))
  })
})
