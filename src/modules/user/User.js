import { Model } from 'objection'
import moment from 'moment'

class User extends Model {
  static tableName = User.name

  $beforeInsert() {
    this.createdAt = moment().unix()
    this.updatedAt = moment().unix()
  }

  $beforeUpdate() {
    this.updatedAt = moment().unix()
  }
}

export default User
