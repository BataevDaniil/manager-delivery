import * as R from 'ramda'
import jwt from 'jsonwebtoken'

import { post, withBody, K, ENV } from '../../../backend'
import User from './User'
import { signinValidator, registrationValidator } from './validators'
import { pipeP } from '../../utils'

export const signin = R.compose(
  post('/signin'),
  withBody({ schema: signinValidator }),
  //
)(({ body, res }) =>
  pipeP(
    R.pipe(
      () => User,
      K.query(),
      K.findOne(body),
    ),
    ({ id } = {}) =>
      id
        ? { token: jwt.sign({ id }, ENV.JWT_TOKEN) }
        : (res.status(400), undefined),
  )(),
)

export const registration = R.compose(
  post('/registration'),
  withBody({ schema: registrationValidator }),
  //
)(({ body }) =>
  R.pipe(
    () => User,
    K.query(),
    K.insert(body),
  )(),
)
