export const user = [
  'firstName',
  'lastName',
  'fatherName',
  'phone',
  'address',
  'countLabels',
  'createdAt',
  'updatedAt',
].map(field => `user.${field}`)
