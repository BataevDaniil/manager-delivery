import jwt from 'jsonwebtoken'

import { ManagerSeed, awaitStartServer, ENV } from '../../../backend'
import * as UserManager from './UserManager'

describe('UserController', () => {
  beforeEach(async () => {
    await awaitStartServer()
  })
  afterEach(async () => {
    await ManagerSeed.deleteAllData()
  })
  it('should signin', async () => {
    const user = { id: 1, email: 'email@gmail.com', password: 'qwerty' }
    await ManagerSeed.insert({
      User: [user],
    })
    const { token } = await UserManager.signin({
      email: user.email,
      password: user.password,
    })

    expect(token).toBe(jwt.sign({ id: user.id }, ENV.JWT_TOKEN))
  })
  it('should registration', async () => {
    await ManagerSeed.insert({})
    const user = { email: 'email@gmail.com', password: 'qwerty' }
    await UserManager.registration({
      email: user.email,
      password: user.password,
    })
    const data = await ManagerSeed.getAllData()
    expect(data.User).toContainEqual(expect.objectContaining(user))
  })
})
