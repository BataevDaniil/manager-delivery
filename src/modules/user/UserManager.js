import fetch from 'node-fetch'
import { pipeP } from '../../utils'

export const signin = pipeP(
  ({ email, password }) =>
    fetch('http://localhost:3000/signin', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    }),
  x => x.json(),
)

export const registration = pipeP(({ email, password }) =>
  fetch('http://localhost:3000/registration', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email,
      password,
    }),
  }),
)
