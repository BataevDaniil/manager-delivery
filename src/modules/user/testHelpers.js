import jwt from 'jsonwebtoken'

import { ENV } from '../../../backend'

export const signin = () => {
  const user = { id: 1, email: 'email@gmail.com', password: 'qwerty' }
  const token = jwt.sign({ id: user.id }, ENV.JWT_TOKEN)
  return { user, token }
}
