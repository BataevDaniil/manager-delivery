import express from 'express'
import passport from 'passport'
import bodyParser from 'body-parser'
import { Model } from 'objection'
import Knex from 'knex'
import * as R from 'ramda'

import knexConfig from '../knexfile'
import * as controllers from './config/controllers'
import { isDevelopment, isProduction } from '../backend'
import { pipeP } from './utils'
import { Order } from './modules/order'
import { User } from './modules/user'

const knex = Knex(isProduction ? knexConfig.production : knexConfig.development)
pipeP(
  () => knex.migrate.latest(),
  () => knex.seed.run(),
)()
Model.knex(knex)

if (isDevelopment) {
  // eslint-disable-next-line
  knex.on('query', ({ sql }) => console.log(`${sql}\n`))
}

const app = express()
app.use(bodyParser.json({}))

app.use(passport.initialize())

const tables = [Order, User]

app.get('/seed', (_, res) =>
  Promise.all(tables.map(table => table.query().eager(), {}))
    .then(data =>
      data.reduce(
        (acc, rows, index) => ({ ...acc, [tables[index].tableName]: rows }),
        {},
      ),
    )
    .then(data => res.status(200).send(data)),
)

app.post('/seed', ({ body }, res) =>
  Promise.all(
    [Order, User].reduce(
      (acc, table) =>
        R.isNil(body[table.tableName])
          ? acc
          : [
              ...acc,
              Promise.all(
                body[table.tableName].map(row =>
                  table.query().insertGraph(row),
                ),
              ),
            ],
      [],
    ),
  ).then(() => res.status(200).send()),
)

app.delete('/seed', (_, res) =>
  pipeP(
    () => knex.migrate.rollback(undefined, true),
    () => knex.migrate.latest(),
    () => res.status(200).send(),
  )(),
)

R.pipe(
  R.values,
  R.chain(R.values),
  R.forEach(x => x(app)),
)(controllers)

app.listen(3000, () => {
  // eslint-disable-next-line
  console.log('Server is listening on port 3000')
})
