import Joi from 'joi'
import * as R from 'ramda'

const withArg = argName => ({
  name = argName,
  schema,
  validate,
} = {}) => func => ctx => {
  const { error: joiError } = schema
    ? Joi.validate(ctx.req[argName], schema, { abortEarly: false })
    : {}
  const error = validate && validate(ctx.req[argName])

  if (joiError || error) {
    ctx.res.status(400)
    return { ...error, ...formatJoiError(joiError) }
  }

  const value = schema
    ? Joi.attempt(ctx.req[argName], schema)
    : ctx.req[argName]
  return func({ ...ctx, [name]: value })
}

export const withBody = withArg('body')
export const withParams = withArg('params')
export const withQuery = withArg('query')
export const withFile = withArg('file')
export const withFiles = withArg('files')

export const paramsToContext = (...config) => fn => (...args) =>
  R.pipe(
    R.addIndex(R.map)((x, i) => ({ [x]: args[i] })),
    R.mergeAll,
    fn,
  )(config)

//TODO: Test code below
export const formatJoiError = R.pipe(
  R.propOr([], 'details'),
  R.reduce(
    (acc = {}, { path, type }) => R.assoc(R.join('.', path), type, acc),
    undefined,
  ),
)
