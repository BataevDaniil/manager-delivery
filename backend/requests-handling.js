import * as R from 'ramda'

const METHODS = {
  GET: 'get',
  POST: 'post',
  PATCH: 'patch',
  PUT: 'put',
  DELETE: 'delete',
}

const handle = method => url => middlewares => router => {
  const adjustMiddleware = func => (req, res, next) => {
    const catchableFunc = ctx => {
      try {
        return Promise.resolve(func(ctx))
      } catch (e) {
        return Promise.reject(e)
      }
    }

    catchableFunc({ req, res, next })
      .then(data => res.send(typeof data === 'number' ? String(data) : data))
      .catch(err => {
        //eslint-disable-next-line
        console.error('Request handling error: ', err)
        if (res.statusCode === 0) {
          res.sendStatus(500)
        } else {
          res.send(err)
        }
      })
  }

  const adjustedMiddlewares = Array.isArray(middlewares)
    ? R.adjust(R.length(middlewares) - 1, adjustMiddleware, middlewares)
    : [adjustMiddleware(middlewares)]

  router[method](url, ...adjustedMiddlewares)
}

export const get = handle(METHODS.GET)
export const post = handle(METHODS.POST)
export const put = handle(METHODS.PUT)
export const patch = handle(METHODS.PATCH)
export const deleteMethod = handle(METHODS.DELETE)

export const addMiddleware = middleware => middlewares =>
  Array.isArray(middlewares)
    ? [middleware, ...middlewares]
    : [middleware, middlewares]

export const status = code => func => ctx => {
  ctx.res.status(code)
  return func(ctx)
}
