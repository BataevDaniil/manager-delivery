import { transaction } from 'objection'

export const transactional = model => func => ctx =>
  transaction(model.knex(), trx => func({ ...ctx, trx }))
