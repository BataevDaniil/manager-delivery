import fetch from 'node-fetch'

export const awaitStartServer = async () => {
  while (true) {
    try {
      await fetch('http://localhost:3000/seed', {
        method: 'GET',
      })
      break
    } catch (e) {
      await new Promise(resolve => setTimeout(resolve, 100))
    }
  }
}
