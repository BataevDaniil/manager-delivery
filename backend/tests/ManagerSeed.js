import fetch from 'node-fetch'

export const getAllData = () =>
  fetch('http://localhost:3000/seed', {
    method: 'GET',
  }).then(x => x.json())

export const deleteAllData = () =>
  fetch('http://localhost:3000/seed', {
    method: 'DELETE',
  })

export const insert = body =>
  fetch('http://localhost:3000/seed', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
