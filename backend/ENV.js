import * as R from 'ramda'
import path from 'path'
import dotenv from 'dotenv'

const current = dotenv.config({
  path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`),
})

const local = dotenv.config({
  path: path.resolve(process.cwd(), '.env.local'),
})

const currentLocal = dotenv.config({
  path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}.local`),
})

const ENV = R.mergeAll(
  [current, local, currentLocal].map(x => (x.error ? {} : x.parsed)),
)

export const isProduction = process.env.NODE_ENV === 'production'
export const isDevelopment = process.env.NODE_ENV === 'development'

export default ENV
